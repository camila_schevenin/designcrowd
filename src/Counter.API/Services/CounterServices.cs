﻿using Nager.Date;
using Nager.Date.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Counter.API.Services
{
    public class CounterServices : ICounterServices
    {
        public int WeekdaysBetweenTwoDates(DateTime firstDate, DateTime secondDate)
        {
            try
            {
                if (secondDate <= firstDate)
                    return 0;

                //Starts tempDate following the next day after the firstDate received
                var tmpDate = firstDate.AddDays(1);
                var count = 0;

                while (tmpDate < secondDate)
                {
                    count = (tmpDate.DayOfWeek != DayOfWeek.Saturday && tmpDate.DayOfWeek != DayOfWeek.Sunday) ? count + 1 : count;
                    tmpDate = tmpDate.AddDays(1);
                }

                return count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int BusinessDaysBetweenTwoDates(DateTime firstDate, DateTime secondDate, IList<DateTime> publicHolidays)
        {
            try
            {
                if (secondDate <= firstDate)
                    return 0;

                //Starts tempDate following the next day after the firstDate received
                var tmpDate = firstDate.AddDays(1);
                var count = 0;

                while (tmpDate < secondDate)
                {
                    count = (tmpDate.DayOfWeek != DayOfWeek.Saturday && tmpDate.DayOfWeek != DayOfWeek.Sunday && !publicHolidays.Contains(tmpDate)) ? count + 1 : count;
                    tmpDate = tmpDate.AddDays(1);
                }

                return count;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<DateTime> GetPublicHoliday()
        {
            try
            {
                using (StreamReader r = File.OpenText("SampleListPublicHoliday.json"))
                {
                    string json = r.ReadToEnd();
                    return JsonConvert.DeserializeObject<List<DateTime>>(json);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }

        public IEnumerable<PublicHoliday> GetPublicHoliday(CountryCode countryCode, DateTime firstDate, DateTime secondDate)
        {
            try
            {
                return DateSystem.GetPublicHoliday(CountryCode.AU, firstDate, secondDate);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
