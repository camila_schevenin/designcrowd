﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Counter.API.Services;
using Microsoft.AspNetCore.Mvc;
using Nager.Date;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Counter.API.Controllers
{
    [Route("api/[controller]/[action]")]
    public class CounterController : Controller
    {
        private readonly ICounterServices _services;

        public CounterController(ICounterServices services)
        {
            _services = services;
        }
        // POST api/<controller>/<action>
        [HttpPost]
        public ActionResult<int> Task1(DateTime firstDate, DateTime secondDate)
        {
            try
            {
                var resultTask1 = _services.WeekdaysBetweenTwoDates(firstDate, secondDate);

                return resultTask1;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // POST api/<controller>/<action>
        [HttpPost]
        public ActionResult<int> Task2(DateTime firstDate, DateTime secondDate)
        {
            try
            {
                var resultTask2 = _services.BusinessDaysBetweenTwoDates(firstDate, secondDate, _services.GetPublicHoliday().ToList());

                return resultTask2;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // POST api/<controller>/<action>
        [HttpPost]
        public ActionResult<int> Task3(DateTime firstDate, DateTime secondDate)
        {
            try
            {
                int resultTask3 = _services.BusinessDaysBetweenTwoDates(firstDate, secondDate, _services.GetPublicHoliday(CountryCode.AU, firstDate, secondDate).Select(p => p.Date).ToList());

                return resultTask3;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
