﻿using Nager.Date;
using Nager.Date.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Counter.API.Services
{
    public interface ICounterServices
    {
        int WeekdaysBetweenTwoDates(DateTime firstDate, DateTime secondDate);
        int BusinessDaysBetweenTwoDates(DateTime firstDate, DateTime secondDate, IList<DateTime> publicHolidays);
        IEnumerable<DateTime> GetPublicHoliday();
        IEnumerable<PublicHoliday> GetPublicHoliday(CountryCode countryCode, DateTime firstDate, DateTime secondDate);
    }
}
